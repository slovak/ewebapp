# Ewebapp
#### Packages
- `Ewebapp/Jetty/`: directory with the servlet container.
- `Ewebapp/WebContent/`: directory with the implementation of the URLs which allow the testing of the backend.
- `Ewebapp/src/ie/logic/`: implementation of the logic layer.
- `Ewebapp/src/ie/dataAccess/`: implementation of the Data Access Layer with its Data Access Objects. The file *tables.sql* was used to define the database. 
- `Ewebapp/src/ie/servlet/`: implementation of the servlets.
- `Ewebapp/src/ie/app/`: simple implementation to test the persistence layer.

#### Instructions to test the backend
- The first step is to start the servlet container (Jetty) to simulate the communication with the backend. To do this run the following commands:
    - `cd Ewebapp/Jetty/demo-base`
    - `java -jar ../start.jar -Djetty.port=8080`
- Next, the implemented operations can be accessed through the URLs:
    - `localhost:8080/Ewebapp/addNewExpense.html`
    - `localhost:8080/Ewebapp/editExpense.html`
    - `localhost:8080/Ewebapp/readExpense.html`

#### Notes
- The database has a id-user-password table with only one available user, so that the authentication is done internally, or not at all. Thus, the User ID field should not be modified or else, the operations will have no effect because they are linked with the connected / existent user credentials.
- Add operation: naive.
- Edit operation:
    The input field **Expense ID to edit**, which refers to the **Id** column in the Expenses table, can be seen after listing the expenses. The editing of an expense with an Id not contained in the table will have no effect over the database.
- Read operation:
    If the three input fields are empty, every expense of the current user is listed. Else, a search is performed depending on the input values.
- The output is quite static, this is, the effects of either adding or editing an expense are seen through listing the objects in the database itself. 

#### Done / LOG
- Authentication.
- Compile into JAR / WAR.
- Somehow deploy the app.
- Controller class.
- Retrieve filtered list of expenses.
- Use empty strings as default strings.
    - Consider having a default value.
- Don't use string concatenation to build queries, use paremeterized queries.
- Set money type to string.
    - Assigned as BigDecimal since its type is NUMERIC in the database.
- Update expense with an ExpenseDescriptor class. Assign ignored attributes to null.
    - Solved using a Properties' class.
