package ie.logic;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.Calendar;

/*
 * @class Expense
 *
 * Describe the expense attributes.
 * */
public class Expense {

    private final String DEFAULT_TEXT = new String("EMPTY");
    private final String DEFAULT_DATE = new String("0000-00-00 00:00:00");

    public enum ExpenseCategory {
        ENTERTAINMENT, FOOD, TRANSPORTATION, SAVINGS, OTHER, DEFAULT_CATEGORY
    }

    // Id is assigned only after insertion
    private long id = 0;
    private String beneficiary;
    private BigDecimal moneyAmount;
    private ExpenseCategory category;
    private String description;
    private String createDate;

    public Expense() {
        this.beneficiary = DEFAULT_TEXT;
        this.moneyAmount = new BigDecimal("0");
        this.category = ExpenseCategory.DEFAULT_CATEGORY;
        this.description = DEFAULT_TEXT;
        this.createDate = DEFAULT_DATE;
    }
    
    public Expense(String beneficiary, BigDecimal moneyAmount, String createDate) {
        this.beneficiary = beneficiary;
        this.moneyAmount = moneyAmount;
        this.category = ExpenseCategory.DEFAULT_CATEGORY;
        this.description = DEFAULT_TEXT;
        this.createDate = createDate;
    }

    public Expense(String beneficiary, BigDecimal moneyAmount, Calendar createDate) {
        this.beneficiary = beneficiary;
        this.moneyAmount = moneyAmount;
        this.category = ExpenseCategory.DEFAULT_CATEGORY;
        this.description = DEFAULT_TEXT;
        this.setCreateDate(createDate);
    }
    
    public Expense(String beneficiary, String moneyAmount, String category, String description, String createDate) {
        this.beneficiary = beneficiary;
        this.moneyAmount = new BigDecimal(moneyAmount.isEmpty() ? "0" : moneyAmount);
        this.setCategory(category);
        this.description = description;
        this.createDate = createDate;
    }

    public void setId(long id) {
        this.id = id;
    }
    
    public void setCategory(String category) {
        this.category = ExpenseCategory.DEFAULT_CATEGORY;
        for (ExpenseCategory c : ExpenseCategory.values()) {
            if (c.name().equals(category.toUpperCase())) {
                this.category = c;
                break;
            }
        }
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setCreateDate(Calendar createDate) {
        this.createDate = String.format("%04d-%02d-%02d %02d:%02d:%02d"
                , createDate.get(Calendar.YEAR)
                , createDate.get(Calendar.MONTH)
                , createDate.get(Calendar.DATE)
                , createDate.get(Calendar.HOUR_OF_DAY)
                , createDate.get(Calendar.MINUTE)
                , createDate.get(Calendar.SECOND));
    }

    public long getId() {
        return this.id;
    }
    
    public String getBeneficiary() {
        return this.beneficiary;
    }

    public BigDecimal getMoneyAmount() {
        return this.moneyAmount;
    }

    public String getCategory() {
        return this.category.name();
    }

    public String getDescription() {
        return this.description;
    }

    public String getCreateDate() {
        return this.createDate;
    }

    public boolean equals(Object o) {
        if (o instanceof Expense) {
            return ((Expense) o).getBeneficiary().equals(this.beneficiary)
                    && ((Expense) o).getMoneyAmount().compareTo(this.moneyAmount) == 0
                    && ((Expense) o).getCategory().equals(this.category.name())
                    && ((Expense) o).getDescription().equals(this.description)
                    && ((Expense) o).getCreateDate().equals(this.createDate);
        }
        return false;
    }

    public String toString() {
        return String.format("Id: %d | Beneficiary: %s | MAmount: %s | "
                + "Category: %s | Description: %s | Date: %s"
                , this.id, this.beneficiary, this.moneyAmount.toString()
                , this.category.name(), this.description, this.createDate);
    }
}
