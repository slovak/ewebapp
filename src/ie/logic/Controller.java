package ie.logic;

import java.util.List;
import java.util.Properties;

import ie.dataAccess.DAL;
import ie.logic.Expense;

/*
 * @class Controller
 *
 * Layer whose purpose is to connect the logic layer with the Data Access Layer.
 * */
public class Controller {

    private static Controller controller = null;
    
    // Empty and private constructor to avoid the creation of multiple Controllers 
    private Controller() {      
    }

    public static Controller getSingletonController() {
        if (controller == null)
            controller = new Controller();
        return controller;
    }
   
    public void createUser(UserInfo user) {
        DAL.getSingleton().createUser(user);
    }

    public void createExpense(UserInfo connectedUser, Expense e) {
        DAL.getSingleton().createExpense(connectedUser, e);
    }

    public void updateExpense(UserInfo connectedUser, long srcExpenseId, Properties updExpense) {
        DAL.getSingleton().updateExpense(connectedUser, srcExpenseId, updExpense);
    }

    public List<Expense> readExpense(UserInfo connectedUser, Properties findExpense) {
        return DAL.getSingleton().readExpense(connectedUser, findExpense);
    }
}
