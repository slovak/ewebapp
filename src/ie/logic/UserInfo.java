package ie.logic;

/*
 * @class UserInfo
 *
 * Describe the user credentials.
 * */
public class UserInfo {

    private long userId;
    private String userName;
    private String userPassword; // Not encrypted

    public UserInfo(String userName, String userPassword) {
        this.userName = userName;
        this.userPassword = userPassword;
    }

    public UserInfo(String userName, String userPassword, long userId) {
        this(userName, userPassword);
        this.userId = userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getUserId() {
        return this.userId;
    }

    public String getUserName() {
        return this.userName;
    }

    public String getUserPassword() {
        return this.userPassword;
    }

    public String toString() {
        return String.format("Id: %d | UserName: %s | Password: %s"
                , this.userId, this.userName, this.userPassword);
    }
}
