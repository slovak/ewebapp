package ie.dataAccess;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ie.logic.Expense;
import ie.logic.UserInfo;

/*
 * @class ExpenseDAO
 *
 * Layer whose purpose is building the PostgreSQL instructions
 * regarding the @class Expense and send them via the connection manager.
 * */
public class ExpenseDAO implements IExpenseDAO {

    public static enum TableExpense {
        // Column name and index.
        ID(2), USER_ID(1), BENEFICIARY(2), MONEY_AMOUNT(3),
        CATEGORY(4), DESCRIPTION(5), CREATE_DATE(6);

        private int columnIndex;
        private TableExpense(int columnIndex) {
            this.columnIndex = columnIndex;
        }

        public int column() {
            return this.columnIndex;
        }
    }

    private ConnectionManager connection;

    public ExpenseDAO() {
        try {
            this.connection = new ConnectionManager();
        }
        catch (ClassNotFoundException cnfe) {
            System.err.println(cnfe.getMessage());
        }
    }

    /*
     * Insert an Expense object in the database.
     *
     * @param connectedUser     the credentials of the connected user
     * @param e                 the properties defining the built object
     * */
    public void createExpense(UserInfo connectedUser, Expense e) {
        //TODO Switch Expense cls to Properties
        String sql = String.format("INSERT INTO"
                + " EXPENSES (%s, %s, %s, %s, %s, %s)"
                + " VALUES (?, ?, ?, ?, ?, ?)"
                , TableExpense.USER_ID.name()
                , TableExpense.BENEFICIARY.name()
                , TableExpense.MONEY_AMOUNT.name()
                , TableExpense.CATEGORY.name()
                , TableExpense.DESCRIPTION.name()
                , TableExpense.CREATE_DATE.name());

        try {
            this.connection.connect();

            PreparedStatement statement = this.connection.buildStatement(sql);

            statement.setLong(TableExpense.USER_ID.column()
                    , connectedUser.getUserId());

            statement.setString(TableExpense.BENEFICIARY.column()
                    , e.getBeneficiary());

            statement.setBigDecimal(TableExpense.MONEY_AMOUNT.column()
                    , e.getMoneyAmount());

            statement.setString(TableExpense.CATEGORY.column()
                    , e.getCategory());

            statement.setString(TableExpense.DESCRIPTION.column()
                    , e.getDescription());

            statement.setString(TableExpense.CREATE_DATE.column()
                    , e.getCreateDate());

            this.connection.updateDb(statement);
            this.connection.close();
        }
        catch (SQLException sqle) {
            System.err.println(sqle.getMessage());
        }
    }

    /*
     * Update an Expense object properties identified by its ID in the database.
     *
     * @param connectedUser     the credentials of the connected user
     * @param srcExpenseId      the ID of the Expense object to be updated
     * @param updExpense        the properties to be updated in the current object
     * */
    public void updateExpense(UserInfo connectedUser, long srcExpenseId, Properties updExpense) {
        // TODO: Allow update not only by ID
        String sql = String.format("SELECT * FROM EXPENSES"
                + " WHERE %s = ? AND %s = ?"
                , TableExpense.USER_ID.name()
                , TableExpense.ID.name());

        try {
            this.connection.connect();

            PreparedStatement statement = this.connection.buildStatement(sql);

            statement.setLong(TableExpense.USER_ID.column(), connectedUser.getUserId());
            statement.setLong(TableExpense.ID.column(), srcExpenseId);

            ResultSet srcExpense = this.connection.queryDb(statement);
            if (!srcExpense.next()) {
                System.err.println(String.format("Cannot UPDATE row with ID"
                        + "=%d from EXPENSES because it is EMPTY.", srcExpenseId));
            }
            else {
                this.updateFromResultSet(srcExpense, updExpense);
            }
            this.connection.close();
        }
        catch (SQLException sqle) {
            System.err.println(sqle.getMessage());
        }
    }

    /*
     * List the Expense objects satisfying the search criteria. Allows filtering
     * by rows { BENEFICIARY , CATEGORY , CREATE_DATE }.
     *
     * @param connectedUser     the credentials of the connected user
     * @param findExpense       the properties to be looked up
     *
     * If findExpense is empty then a every row is selected.
     * */
    public List<Expense> readExpense(UserInfo connectedUser, Properties findExpense) {
        List<Expense> selectResult = new ArrayList<Expense>();
        List<String> columnNames = new ArrayList<String>();
        StringBuilder builderSql = new StringBuilder(
                String.format("SELECT * FROM EXPENSES"
                        + " WHERE %s = ?", TableExpense.USER_ID.name()));

        for (String columnName : findExpense.stringPropertyNames()) {
            if (columnName.isEmpty()) {
                continue;
            }
            builderSql.append(String.format(" AND UPPER(%s) LIKE UPPER(?)", columnName));
            columnNames.add(columnName);
        }
        builderSql.append(";");

        String sql = builderSql.toString();

        try {
            this.connection.connect();

            PreparedStatement statement = this.connection.buildStatement(sql);

            statement.setLong(TableExpense.USER_ID.column(), connectedUser.getUserId());
            String paramValue;
            int paramIndex = 2;
            for (String columnName : columnNames) {
                paramValue = findExpense.getProperty(columnName);
                statement.setString(paramIndex, "%%" + paramValue + "%%");
                paramIndex++;
            }

            ResultSet selectExpense = this.connection.queryDb(statement);
            while (selectExpense.next()) {
                long expenseId = selectExpense.getLong(TableExpense.ID.name());
                String beneficiary = selectExpense.getString(TableExpense.BENEFICIARY.name());
                String moneyAmount = selectExpense.getString(TableExpense.MONEY_AMOUNT.name());
                String category = selectExpense.getString(TableExpense.CATEGORY.name());
                String description = selectExpense.getString(TableExpense.DESCRIPTION.name());
                String createDate = selectExpense.getString(TableExpense.CREATE_DATE.name());

                Expense foundExpense = new Expense(beneficiary, moneyAmount, category, description, createDate);
                foundExpense.setId(expenseId);

                selectResult.add(foundExpense);
            }

            this.connection.close();
        }
        catch (SQLException sqle) {
            System.err.println(sqle.getMessage());
        }

        return selectResult;
    }

    /*
     * Update row in the database with the new Expense properties.
     *
     * @param srcExpense    object referring to the updatable row
     * @param updExpense    the properties to be updated in the current object
     * */
    private void updateFromResultSet(ResultSet srcExpense, Properties updExpense)
        throws SQLException {
        String updValue;
        for (String property : updExpense.stringPropertyNames()) {
            updValue = updExpense.getProperty(property);
            if (updValue.isEmpty()) {
                continue;
            }
            else if (property.equals(TableExpense.MONEY_AMOUNT.name())) {
                BigDecimal castMoneyAmount = new BigDecimal(updValue);
                srcExpense.updateBigDecimal(TableExpense.MONEY_AMOUNT.name()
                        , castMoneyAmount);
            }
            else {
                srcExpense.updateString(property, updValue);
            }
        }

        srcExpense.updateRow();
    }
}
