package ie.dataAccess;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;

/*
 * @class ConnectionManager
 *
 * Layer used by Data Access Objects whose purpose is sending
 * PostgreSQL instructions to the database.
 * */
public class ConnectionManager {

    private String sourceURL;
    private String dbUsername;
    private String dbPassword;
    private Connection dbConnection = null;

    public ConnectionManager() throws ClassNotFoundException {
        Class.forName("org.postgresql.Driver");
        this.sourceURL = "jdbc:postgresql://horton.elephantsql.com/ofzgcnel";
        this.dbUsername = "ofzgcnel";
        this.dbPassword = "o33eu892tT42rylzrVIFjr2h6PfTNKN1";
    }

    public void connect() throws SQLException {
        if (this.dbConnection == null) {
            this.dbConnection = DriverManager.getConnection(sourceURL, this.dbUsername, this.dbPassword);
        }
    }

    public void close() throws SQLException {
        if (this.dbConnection != null) {
            this.dbConnection.close();
            this.dbConnection = null;
        }
    }

    public PreparedStatement buildStatement(String parameterizedSql) {
        PreparedStatement statement = null;
        if (this.dbConnection != null) {
            try {
                statement = this.dbConnection.prepareStatement(
                        parameterizedSql
                        , ResultSet.TYPE_FORWARD_ONLY
                        , ResultSet.CONCUR_UPDATABLE);
            }
            catch (SQLException sqle) {
                System.err.println(sqle.getMessage());
            }
        }
        return statement;
    }

    public void updateDb(PreparedStatement statement) throws SQLException {
        if (this.dbConnection != null) {
            statement.executeUpdate();
        }
    }

    public ResultSet queryDb(PreparedStatement statement) throws SQLException {
        if (this.dbConnection != null) {
            return statement.executeQuery();
        }
        return null;
    }
}
