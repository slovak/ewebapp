package ie.dataAccess;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import ie.logic.UserInfo;

/*
 * @class UserInfoDAO
 *
 * Layer whose purpose is building the PostgreSQL instructions
 * regarding the @class UserInfo and send them via the connection manager.
 * */
public class UserInfoDAO implements IUserInfoDAO {

    private enum TableUserInfo {
        // Column name and index.
        ID(1), NAME(1), PASSWORD(2);

        private int columnIndex;
        private TableUserInfo(int columnIndex) {
            this.columnIndex = columnIndex;
        }

        public int column() {
            return this.columnIndex;
        }
    }

    private ConnectionManager connection;

    public UserInfoDAO() {
        try {
            this.connection = new ConnectionManager();
        }
        catch (ClassNotFoundException cnfe) {
            System.err.println(cnfe.getMessage());
        }
    }

    /*
     * Insert a UserInfo object in the database.
     *
     * @param user  the credentials of the user
     * */
    public void createUser(UserInfo user) {
        if (this.containsUserInTable(user)) {
            System.err.println(String.format("User with NAME=%s already"
                    + " exists in the table USERINFO"
                    , user.getUserName()));
            return;
        }

        String sql = String.format("INSERT INTO"
                + " USERINFO (%s, %s)"
                + " VALUES (?, ?)"
                , TableUserInfo.NAME.name()
                , TableUserInfo.PASSWORD.name());

        try {
            this.connection.connect();

            PreparedStatement statement = this.connection.buildStatement(sql);

            statement.setString(TableUserInfo.NAME.column()
                    , user.getUserName());

            statement.setString(TableUserInfo.PASSWORD.column()
                    , user.getUserPassword());

            this.connection.updateDb(statement);

            user.setUserId(this.readUserId(user));

            this.connection.close();
        }
        catch (SQLException sqle) {
            System.err.println(sqle.getMessage());
        }
    }

    /*
     * Check if a user with the same name exists in the table.
     *
     * @param user   the credentials of the user
     * @return true  if exists
     * */
    private boolean containsUserInTable(UserInfo user) {
        String sql = String.format("SELECT %s"
                + " FROM USERINFO"
                + " WHERE %s = ?"
                , TableUserInfo.ID.name()
                , TableUserInfo.NAME.name()
                , user.getUserName());

        try {
            this.connection.connect();

            PreparedStatement statement = this.connection.buildStatement(sql);

            statement.setString(TableUserInfo.NAME.column()
                    , user.getUserName());

            ResultSet selectId = this.connection.queryDb(statement);

            this.connection.close();

            if (selectId.next()) {
                long userId = selectId.getLong(TableUserInfo.ID.column());

                user.setUserId(userId);

                return true;
            }
        }
        catch (SQLException sqle) {
            System.err.println(sqle.getMessage());
        }

        return false;
    }

    /*
     * Select user's private ID.
     *
     * @param user   the credentials of the user
     * */
    private long readUserId(UserInfo user) {
        long userId = 0L;
        String sql = String.format("SELECT %s FROM USERINFO"
                + " WHERE NAME = ?", TableUserInfo.ID.name());

        try {
            this.connection.connect();

            PreparedStatement statement = this.connection.buildStatement(sql);

            statement.setString(TableUserInfo.NAME.column()
                    , user.getUserName());

            ResultSet selectId = this.connection.queryDb(statement);
            if (!selectId.next()) {
                System.err.println(String.format("Cannot READ row with NAME"
                        + "=%s from USERINFO because it is EMPTY."
                        , user.getUserName()));
            }
            else {
                userId = selectId.getLong(TableUserInfo.ID.column());
            }

            this.connection.close();
        }
        catch (SQLException sqle) {
            System.err.println(sqle.getMessage());
        }

        return userId;
    }
}
