package ie.dataAccess;

import java.util.List;
import java.util.Properties;

import ie.logic.Expense;
import ie.logic.UserInfo;

public interface IExpenseDAO {

    /* Insert an Expense object in the database. */
    public void createExpense(UserInfo connectedUser, Expense e);
    /* Update an Expense object properties identified by its ID in the database. */
    public void updateExpense(UserInfo connectedUser, long srcExpenseId, Properties updExpense);
    /* List the Expense objects satisfying the search criteria. */
    public List<Expense> readExpense(UserInfo connectedUser, Properties findExpense);
}
