package ie.dataAccess;

import ie.logic.UserInfo;

public interface IUserInfoDAO {

    /* Insert a UserInfo object in the database. */
    public void createUser(UserInfo user);
}
