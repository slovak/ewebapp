package ie.dataAccess;

import java.util.List;
import java.util.Properties;

import ie.logic.Expense;
import ie.logic.UserInfo;

/*
 * @class DAL
 *
 * Data Access Layer (DAL) whose purpose is managing the work flow 
 * between the database and the logic layer.
 * 
 * TODO: Assert each operation.
 * */
public class DAL {

    private static DAL dal = null;
    
    // Empty and private constructor to avoid the creation of multiple DALs
    private DAL() {
    }

    public static DAL getSingleton() {
        if (dal == null)
            dal = new DAL();
        return dal;
    }
    
    public void createUser(UserInfo user) {
        (new UserInfoDAO()).createUser(user);
    }

    public void createExpense(UserInfo connectedUser, Expense e) {
        (new ExpenseDAO()).createExpense(connectedUser, e);
    }

    public void updateExpense(UserInfo connectedUser, long srcExpenseId, Properties updExpense) {
        (new ExpenseDAO()).updateExpense(connectedUser, srcExpenseId, updExpense);
    }

    public List<Expense> readExpense(UserInfo connectedUser, Properties findExpense) {
        return (new ExpenseDAO()).readExpense(connectedUser, findExpense);
    }
}
