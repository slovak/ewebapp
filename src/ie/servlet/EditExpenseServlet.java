package ie.servlet;

import java.io.IOException;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ie.dataAccess.ExpenseDAO;
import ie.logic.Controller;
import ie.logic.UserInfo;

/*
 * @class EditExpenseServlet
 *
 * Implement the use case to edit an expense.
 * */
@WebServlet("/editExpenseServlet")
public class EditExpenseServlet extends HttpServlet {
       
    public EditExpenseServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String attrUserId = request.getParameter("attrUserId");
        String attrUserName = request.getParameter("attrUserName");
        String attrUserPassword = request.getParameter("attrUserPassword");
        String attrExpenseId = request.getParameter("attrExpenseId");
        String attrBeneficiary = request.getParameter("attrBeneficiary");
        String attrMoneyAmount = request.getParameter("attrMoneyAmount");
        String attrCategory = request.getParameter("attrCategory");
        String attrDescription = request.getParameter("attrDescription");
        String attrCreateDate = request.getParameter("attrCreateDate");
             
        long connectedUserId = Long.parseLong(attrUserId);
        UserInfo connectedUser = new UserInfo(attrUserName, attrUserPassword, connectedUserId);
        
        long editExpenseId = Long.parseLong(attrExpenseId);
        Properties editExpense = new Properties();
        editExpense.setProperty(ExpenseDAO.TableExpense.BENEFICIARY.name(), attrBeneficiary);
        editExpense.setProperty(ExpenseDAO.TableExpense.MONEY_AMOUNT.name(), attrMoneyAmount);
        editExpense.setProperty(ExpenseDAO.TableExpense.CATEGORY.name(), attrCategory.toUpperCase());
        editExpense.setProperty(ExpenseDAO.TableExpense.DESCRIPTION.name(), attrDescription);
        editExpense.setProperty(ExpenseDAO.TableExpense.CREATE_DATE.name(), attrCreateDate);
        
        Controller.getSingletonController().updateExpense(connectedUser, editExpenseId, editExpense);
        
        response.getWriter().println(String.format("Edit expense with id\n\t%d\n", editExpenseId));
        response.getWriter().println("with new data:");
        for (String property : editExpense.stringPropertyNames()) {
            response.getWriter().println(String.format("\t%s : %s\n", property, editExpense.getProperty(property)));
        }
        response.getWriter().println(String.format("by User:\n\t%s\n", connectedUser.toString()));
    }
}
