package ie.servlet;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ie.dataAccess.ExpenseDAO;
import ie.logic.Controller;
import ie.logic.Expense;
import ie.logic.UserInfo;

/*
 * @class ReadExpenseServlet
 *
 * Implement the use case to read an expense.
 * */
@WebServlet("/readExpenseServlet")
public class ReadExpenseServlet extends HttpServlet {
   
    public ReadExpenseServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String attrUserId = request.getParameter("attrUserId");
        String attrUserName = request.getParameter("attrUserName");
        String attrUserPassword = request.getParameter("attrUserPassword");
        String attrBeneficiary = request.getParameter("attrBeneficiary");
        String attrCategory = request.getParameter("attrCategory");
        String attrCreateDate = request.getParameter("attrCreateDate");
             
        long connectedUserId = Long.parseLong(attrUserId);
        UserInfo connectedUser = new UserInfo(attrUserName, attrUserPassword, connectedUserId);
        
        Properties findExpense = new Properties();
        findExpense.setProperty(ExpenseDAO.TableExpense.BENEFICIARY.name(), attrBeneficiary);
        findExpense.setProperty(ExpenseDAO.TableExpense.CATEGORY.name(), attrCategory);
        findExpense.setProperty(ExpenseDAO.TableExpense.CREATE_DATE.name(), attrCreateDate);
        
        List<Expense> resultQuery = Controller.getSingletonController().readExpense(connectedUser, findExpense);
        
        response.getWriter().println("Find expense with properties:");
        for (String property : findExpense.stringPropertyNames()) {
            response.getWriter().println(String.format("\t%s : %s\n", property, findExpense.getProperty(property)));
        }
        response.getWriter().println(String.format("by User:\n\t%s\n", connectedUser.toString()));
        response.getWriter().println(String.format("with %d results", resultQuery.size()));
        for (Expense e : resultQuery) {
            response.getWriter().println(String.format("\t%s\n", e.toString()));
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }
}
