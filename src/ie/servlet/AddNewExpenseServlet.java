package ie.servlet;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import ie.logic.Controller;
import ie.logic.Expense;
import ie.logic.UserInfo;

/*
 * @class AddNewExpenseServlet
 *
 * Implement the use case to add a new expense.
 * */
@WebServlet("/addNewExpenseServlet")
public class AddNewExpenseServlet extends HttpServlet {
    
    public AddNewExpenseServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String attrUserId = request.getParameter("attrUserId");
        String attrUserName = request.getParameter("attrUserName");
        String attrUserPassword = request.getParameter("attrUserPassword");
        String attrBeneficiary = request.getParameter("attrBeneficiary");
        String attrMoneyAmount = request.getParameter("attrMoneyAmount");
        String attrCategory = request.getParameter("attrCategory");
        String attrDescription = request.getParameter("attrDescription");
        String attrCreateDate = request.getParameter("attrCreateDate");
             
        long connectedUserId = Long.parseLong(attrUserId);
        UserInfo connectedUser = new UserInfo(attrUserName, attrUserPassword, connectedUserId);
        
        Expense addExpense = new Expense(attrBeneficiary, attrMoneyAmount, attrCategory, attrDescription, attrCreateDate);
        
        Controller.getSingletonController().createExpense(connectedUser, addExpense);
        
        response.getWriter().println(String.format("Add expense:\n\t%s\n", addExpense.toString()));
        response.getWriter().println(String.format("by User:\n\t%s\n", connectedUser.toString()));
    }
}
