package ie.app;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

import ie.dataAccess.*;
import ie.logic.*;

public class App {

    public static void main(String [] args) {
        UserInfo ui = new UserInfo("slovak", "+pan-chorizo");
        System.out.println(ui.toString());
        Expense ea = new Expense("Beneficiary002", new BigDecimal("-1000.25"), Calendar.getInstance());
        Expense eb = new Expense("Beneficiary003", new BigDecimal("1000.25"), Calendar.getInstance());
        eb.setCategory("FOOD");
        System.out.println(ea.toString());
        System.out.println(eb.toString());

        Properties pa = new Properties();
        pa.setProperty("BENEFICIARY", "DROP TABLE EXPENSES;--");
        pa.setProperty("MONEY_AMOUNT", "-100.00");

        Properties pb = new Properties();
        pb.setProperty("BENEFICIARY", "Vlad001");
        pb.setProperty("CATEGORY", "ENTERTAINMENT");

        Properties pc = new Properties();
        pc.setProperty("BENEFICIARY", "Benef");

        Properties pd = new Properties();

        Controller.getSingletonController().createUser(ui);
        Controller.getSingletonController().createExpense(ui, ea);
        Controller.getSingletonController().createExpense(ui, eb);

        List<Expense> expensesb = Controller.getSingletonController().readExpense(ui, pc);
        for (Expense ex : expensesb) {
            System.err.println(ex.toString());
        }

        Controller.getSingletonController().updateExpense(ui, 5, pa);
        Controller.getSingletonController().updateExpense(ui, 1, pb);

        List<Expense> expensesc = Controller.getSingletonController().readExpense(ui, pd);
        for (Expense ex : expensesc) {
            System.err.println(ex.toString());
        }
    }
}
